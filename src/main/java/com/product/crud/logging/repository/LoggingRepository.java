package com.product.crud.logging.repository;

import com.product.crud.logging.LogEntry;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LoggingRepository extends MongoRepository<LogEntry,String>  {
	
}
