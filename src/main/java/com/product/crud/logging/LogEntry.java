package com.product.crud.logging;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class LogEntry {
	
	public LogEntry(String id,String process,String ip,boolean success){
		this.id=id;
		this.process=process;
		this.ip=ip;
		this.success=success;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
	private String process;
	private String ip;
	private boolean success;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
}
