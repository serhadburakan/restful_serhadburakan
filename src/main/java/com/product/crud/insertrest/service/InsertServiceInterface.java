package com.product.crud.insertrest.service;

import com.product.crud.product.Product;

public interface InsertServiceInterface {
		
	Product create(Product product,String ip);
	
}
