package com.product.crud.insertrest.service;

import java.text.SimpleDateFormat;
import javax.sql.DataSource;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import com.product.crud.insertrest.loggingservice.LoggingService;
import com.product.crud.logging.LogEntry;
import com.product.crud.product.Product;

@Service
public class InsertService implements InsertServiceInterface {


	private JdbcTemplate jdbcTemplate; 
	
	@Autowired
	LoggingService loggingService;
	
	 @Autowired
	    public void setDataSource(DataSource dataSource) {
	        this.jdbcTemplate = new JdbcTemplate(dataSource);
	    }
	
	 public Product create(Product product,String ip){
			
			try{
				SimpleDateFormat sdf=new SimpleDateFormat("dd.MM.yyyy");
			this.jdbcTemplate.update(
			        "insert into product (id,name,detail,color,size) values (?,?,?,?,?)",
			        product.getId(),product.getName(),product.getDetail(),product.getColor(),product.getSize());
			
			this.jdbcTemplate.update(
			        "insert into price (productid,productprice,begindate,enddate) values (?,?,?,?)",
			        product.getId(),product.getPrice().getProductPrice(),sdf.parse(product.getPrice().getBeginDate()),sdf.parse(product.getPrice().getEndDate()));
				product.setSuccess(true);	
			}
			catch(Exception e){
				product.setSuccess(false);
				product.setMessage(e.getMessage());
			}
			
			loggingService.save(new LogEntry(new ObjectId().toString(),"insert" ,ip,product.isSuccess()));
			
			return product;
			
		}
	
	
	
}
