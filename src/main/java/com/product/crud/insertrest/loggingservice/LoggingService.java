package com.product.crud.insertrest.loggingservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.product.crud.logging.LogEntry;
import com.product.crud.logging.repository.LoggingRepository;

@Service
public class LoggingService {

	@Autowired
	LoggingRepository loggingRepository;
	
	public void save(LogEntry logEntry){
		loggingRepository.save(logEntry);
	}
	
}
