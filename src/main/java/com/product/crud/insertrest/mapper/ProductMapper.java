package com.product.crud.insertrest.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import org.springframework.jdbc.core.RowMapper;
import com.product.crud.product.Price;
import com.product.crud.product.Product;

public final class ProductMapper implements RowMapper<Product> {

	public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
		Product product = new Product();
		product.setId(rs.getLong("id"));
		product.setName(rs.getString("name"));
		product.setDetail(rs.getString("detail"));
		product.setColor(rs.getString("color"));
		product.setSize(rs.getInt("size"));
		Price price=new Price();
	
		price.setProductPrice(rs.getFloat("productprice"));
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
		price.setBeginDate(rs.getDate("begindate").toString());
		price.setEndDate(rs.getDate("enddate").toString());
		
		product.setPrice(price);
		return product;
	}
}
