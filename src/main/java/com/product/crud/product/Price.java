package com.product.crud.product;

import com.fasterxml.jackson.annotation.JsonRootName;


@JsonRootName(value = "price")
public class Price {

	
	
	
	private float productPrice;
	
	
	private String beginDate;
	
	
	private String endDate;

	
	

	public float getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(float productPrice) {
		this.productPrice = productPrice;
	}

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	
	
}
