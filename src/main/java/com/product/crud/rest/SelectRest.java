package com.product.crud.rest;


import java.util.concurrent.Callable;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.product.crud.product.Product;
import com.product.crud.selectrest.service.SelectService;

@RestController
public class SelectRest {

	@Autowired
	SelectService selectService;
	

	
	@RequestMapping(value="/get/{id}", method=RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Callable<Product> find(@PathVariable("id") long id, HttpServletRequest request ){
		
		Callable<Product> task = () -> {

	return selectService.findById(id,request.getRemoteAddr());
	
		};
		
		return task;
	}
	
		
}
