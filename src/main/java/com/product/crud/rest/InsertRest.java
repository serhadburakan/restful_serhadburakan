package com.product.crud.rest;


import java.util.concurrent.Callable;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.product.crud.insertrest.service.InsertService;
import com.product.crud.product.Price;
import com.product.crud.product.Product;

@RestController
public class InsertRest {

	
	
	@Autowired
	InsertService insertService;
	
	


	@RequestMapping(value="/create/{id}/{name}/{detail}/{size}/{color}/{productPrice}/{beginDate}/{endDate}", method=RequestMethod.GET,produces="application/json")
	@ResponseBody
	public Callable<Product> create(@PathVariable("id") long id,@PathVariable("name") String name,
			@PathVariable("detail") String detail,@PathVariable("size") int size,
			@PathVariable("color") String color,
			@PathVariable("productPrice") float productPrice,@PathVariable("beginDate") String beginDate,@PathVariable("endDate") String endDate, HttpServletRequest request ){
		
		Product product=new Product();
		
	
		Callable<Product> task = () -> {
		
		if(id==0||name==null||detail==null){
		product.setSuccess(false);
		product.setMessage("id, name and detail cannot be null");
		return product;
		}
		
		
		try{
		
		product.setId(id);
		product.setName(name);
		product.setDetail(detail);
		product.setSize(size);
		product.setColor(color);
		Price price=new Price();
		
		price.setProductPrice(productPrice);
		
        
		price.setBeginDate(beginDate);
		price.setEndDate(endDate);
		product.setPrice(price);
		return insertService.create(product,request.getRemoteAddr());
				}
		catch(Exception e){
			product.setSuccess(false);
			product.setMessage(e.getMessage());
			return product;
		}
		
	
	
		};
		
		return task;
		
	}

	
}
