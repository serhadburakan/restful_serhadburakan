package com.product.crud.selecttest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.sql.DataSource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.product.crud.InsertSelectProductApplicationTests;

public class SelectTest extends InsertSelectProductApplicationTests {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Before
	public void beforeTest() throws DataAccessException, ParseException {

		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

		this.jdbcTemplate.update("delete from Price");
		this.jdbcTemplate.update("delete from Product");

		for (long i = 1; i <= 100; i++) {
			this.jdbcTemplate.update("insert into product (id,name,detail,color,size) values (?,?,?,?,?)", i,
					"test" + i, "detail" + i, "k�rm�z�" + i, (int) i);
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			this.jdbcTemplate.update("insert into price (productid,productprice,begindate,enddate) values (?,?,?,?)", i,
					100 + i, sdf.parse("01.01.2015"), sdf.parse("31.12.2015"));

		}
	}

	@Test
	public void selectTestMethodSuccess() throws Exception {

		for (int i = 1; i <= 100; i++) {

			MvcResult mvcResult = this.mockMvc.perform(get("/get/" + i)).andExpect(request().asyncStarted())
					.andReturn();

			ResultActions resultActions = mockMvc.perform(asyncDispatch(mvcResult));
			resultActions.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("success").value(true)).andExpect(jsonPath("message").value("success"))
					.andExpect(jsonPath("id").value(i)).andExpect(jsonPath("name").value("test" + i))
					.andExpect(jsonPath("detail").value("detail" + i));

			// resultActions.andDo(MockMvcResultHandlers.print());

		}

	}
	
	
	@Test
	public void selectTestMethodError() throws Exception {

		for (int i = 101; i <= 100; i++) {

			MvcResult mvcResult = this.mockMvc.perform(get("/get/" + i)).andExpect(request().asyncStarted())
					.andReturn();

			ResultActions resultActions = mockMvc.perform(asyncDispatch(mvcResult));
			resultActions.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
					.andExpect(jsonPath("success").value(false));


		}

	}
	

	@After
	public void afterTest() {
		// this.jdbcTemplate.update("delete from Price");
		// this.jdbcTemplate.update("delete from Product");
	}

}
